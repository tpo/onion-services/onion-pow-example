#!/usr/bin/env python3

import http.server
import stem.control
import stem.util.conf
import argparse, requests, asyncio, ipaddress, struct, threading, random
import os, time, html, io, re, sys


class HTTPServer(http.server.HTTPServer):
    def page_request(self, request):
        circ_info = self.circ_info.get(request.circuit_id)
        circ_log = circ_info and "\n".join(
            f"[{i.type}] {repr(i.keyword_args)}" for i in circ_info.log
        )
        circ_request_counter = circ_info and circ_info.count_request()
        circ_pow = (
            circ_info and circ_info.log and circ_info.log[-1].keyword_args.get("HS_POW")
        )
        pqueue_count = self.metrics.int_value("tor_hs_rdv_pow_pqueue_count")
        suggested_effort = self.metrics.int_value("tor_hs_pow_suggested_effort")

        if circ_pow and circ_pow.startswith("v1,"):
            summary_class, summary_heading = "ok", "Ok!"
            circ_pow_effort = int(circ_pow.split(",")[1])
            assert circ_pow_effort > 0
            summary_text = f"Your client has successfully connected using a proof-of-work puzzle, with effort {circ_pow_effort}."

        elif pqueue_count < self.loadgen.pqueue_min_level:
            summary_class, summary_heading = "none", "Inconclusive"
            summary_text = "The service's request queue is nearly empty. The load generator should be working on filling it now."

        elif suggested_effort < 1:
            summary_class, summary_heading = "none", "Inconclusive"
            summary_text = "The service hasn't yet enabled PoW mitigations. Give it a few minutes and try again."

        else:
            summary_class, summary_heading = "err", "Unsupported"
            summary_text = "You've connected successfully, but without providing a proof-of-work solution."

        esc = lambda s: html.escape(str(s))
        css = re.sub(r"\s+", " ", self.css)
        return f"""{''
            }<!DOCTYPE HTML>\n{''
            }<html><head>{''
                }<meta name="viewport" content="width=device-width, initial-scale=1" />{''
                }<title>{esc(self.title)}</title>{''
                }<style>{css}</style>{''
            }</head><body>{''
                }<h1>{esc(self.title)}</h1>{''
                }<h2 class="summary summary-{esc(summary_class)}">{
                    esc(summary_heading)
                }</h2>{''
                }<p>{esc(summary_text)}</p>{''
                }<pre>{''
                    }Server time: {
                        esc(time.ctime())}\n{''
                    }Current count of queued requests: {
                        esc(pqueue_count)}\n{''
                    }Current suggested effort: {
                        esc(suggested_effort)}\n{''
                    }Request counter for this circuit: {
                        esc(circ_request_counter)}\n{''
                    }Validated client PoW (type,effort) for this circuit: {
                        esc(circ_pow)
                }</pre>{''
                }<h2>Service configuration</h2>{''
                }<pre>{esc(self.config_text)}</pre>{''
                }<h2>Circuit log</h2>{''
                }<pre>{''
                    }#{esc(request.circuit_id)}\n{''
                    }{esc(circ_log)}{''
                }</pre>{''
            }</body></html>"""

    title = "Onion Proof-of-Work Example"
    css = """
        body {
            font: 0.8em sans-serif;
        }
        .summary {
            padding: 0.4em;
            border: 1.5px solid rgba(255,255,255,0.1);
        }
        .summary-none {
            background: #ffd25a;
        }
        .summary-ok {
            background: #80cfa9;
        }
        .summary-err {
            background: #c65b7c;
        }
    """

    def do_CIRC(self, event):
        info = self.circ_info.setdefault(int(event.id), CircuitInfo(int(event.id)))
        info.log.append(event)

    def do_CIRC_MINOR(self, event):
        self.circ_info[int(event.id)].log.append(event)

    def __init__(self, http_port, controller, config_text):
        http.server.HTTPServer.__init__(self, ("127.0.0.1", http_port), RequestHandler)
        controller.authenticate()
        for name in dir(self):
            m = re.match(r"^do_([A-Z_]+)$", name)
            if m:
                controller.add_event_listener(getattr(self, name), m.group(1))

        self.config_text = config_text
        self.circ_info = {}


class MetricsPoller(threading.Thread):
    def __init__(self, metrics_port):
        threading.Thread.__init__(self)
        self.debug_watchlist = set()
        self.url = f"http://127.0.0.1:{metrics_port}/metrics"
        self.items = {}
        self.start()

    def __getitem__(self, name):
        return self.items.get(name)

    def single_value(self, name, default=None):
        self.debug_watchlist.add(name)
        if name in self.items:
            all = list(self.items[name].values())
            if len(all) == 1:
                return all[0]
        return default

    def int_value(self, name, default=0):
        return int(self.single_value(name, default))

    def run(self):
        time.sleep(2)
        while True:
            time.sleep(1)
            self.poll()

    def poll(self):
        new_items = {}
        for line in requests.get(self.url).text.split("\n"):
            line = line.strip()
            if line.startswith("#"):
                continue
            m = re.match(r"^(\w+){([^{}]*)} +(.*)$", line)
            if m:
                metric, tags, value = m.groups()
                new_items.setdefault(metric, {})[tags] = value
                if metric in self.debug_watchlist:
                    print(f"[metrics] {metric} {tags} {value}")
        self.items = new_items


class LoadGenerator(threading.Thread):
    def __init__(
        self,
        metrics,
        onion,
        socks_port,
        timeout=60,
        num_connections=90,
        pqueue_min_level=10,
    ):
        threading.Thread.__init__(self)
        self.metrics = metrics
        self.onion = onion
        self.socks_port = socks_port
        self.timeout = timeout
        self.num_connections = num_connections
        self.delay_per_start = self.timeout / self.num_connections
        self.no_start_until_time = time.time()
        self.pqueue_min_level = pqueue_min_level
        self.unique_id = random.randint(100000, 999999)
        self.start()

    @staticmethod
    def socks4a_header(port, user_id, hostname):
        return (
            struct.pack("!BBHI", 4, 1, port, 1)
            + user_id.encode()
            + b"\x00"
            + hostname.encode()
            + b"\x00"
        )

    def build_message(self):
        assert self.onion.endswith(".onion")
        self.unique_id += 1
        return (
            self.socks4a_header(80, str(self.unique_id), self.onion)
            + b"GET /load\r\n\r\n"
        )

    def log_status(self, connection_id, state_message):
        print(f"[loadgen {connection_id}] {state_message}")

    async def single_connection(self, connection_id):
        while True:
            # Wait if we have a delay set
            now = time.time()
            delay = self.no_start_until_time - now
            if delay > 0:
                await asyncio.sleep(delay)
                continue

            # Throttle the load generator when our single pqueue crosses
            # twice the minimum level threshold and becomes reliably non-empty.
            pqueue_count = self.metrics.int_value("tor_hs_rdv_pow_pqueue_count")
            if pqueue_count >= self.pqueue_min_level * 2:
                await asyncio.sleep(1)
                continue

            # Every connection launch delays future connections in order to
            # keep them spread out across our timeout period
            self.no_start_until_time = now + self.delay_per_start

            self.log_status(connection_id, "starting")
            try:
                async with asyncio.timeout(self.timeout):
                    reader, writer = await asyncio.open_connection(
                        "127.0.0.1", self.socks_port
                    )
                    try:
                        writer.write(self.build_message())
                        await writer.drain()
                        await reader.read(16)
                    finally:
                        writer.close()
                    await writer.wait_closed()
                    self.log_status(connection_id, "complete")
            except asyncio.TimeoutError:
                self.log_status(connection_id, "timeout")

    async def parallel_connections(self):
        await asyncio.gather(
            *(self.single_connection(i) for i in range(self.num_connections))
        )

    def run(self):
        asyncio.run(self.parallel_connections())


class RequestHandler(http.server.BaseHTTPRequestHandler):
    circuit_id = None

    def send_head(self, response):
        self.send_response(http.server.HTTPStatus.OK)
        self.send_header("Content-type", "text/html; charset=utf8")
        self.send_header("Content-Length", str(len(response)))

    def page_request(self):
        return self.server.page_request(self).encode("utf8")

    def parse_request(self):
        if self.raw_requestline.startswith(b"PROXY"):
            self.do_PROXY()
            return False
        else:
            return http.server.BaseHTTPRequestHandler.parse_request(self)

    def do_PROXY(self):
        requestline = str(self.raw_requestline, "iso-8859-1")
        words = requestline.split()
        assert words[0] == "PROXY" and words[1] == "TCP6"
        from_addr = ipaddress.IPv6Address(words[2])
        self.circuit_id = struct.unpack(">IIII", from_addr.packed)[3]
        self.handle_one_request()

    def do_HEAD(self):
        self.send_head(self.page_request())

    def do_GET(self):
        r = self.page_request()
        self.send_head(r)
        self.wfile.write(r)


class CircuitInfo:
    def __init__(self, id):
        self.id = id
        self.counter = 0
        self.log = []

    def count_request(self):
        self.counter += 1
        return self.counter


def main(config_file):
    config = stem.util.conf.get_config("tor")
    config.load(config_file)
    control_port = int(config.get("ControlPort"))
    http_port = int(config.get("HiddenServicePort").split(":")[1])
    metrics_port = int(config.get("MetricsPort"))
    socks_port = int(config.get("SocksPort"))
    onion_file = os.path.join(config.get("HiddenServiceDir"), "hostname")
    onion = open(onion_file).read().strip()
    config_text = "\n".join(
        f"{k} {config.get(k)}" for k in config.keys() if k.startswith("HiddenService")
    )

    print(f"\n{'='*10} http://{onion} {'='*10}\n")

    with stem.control.Controller.from_port("127.0.0.1", control_port) as c:
        httpd = HTTPServer(http_port, c, config_text)
        httpd.metrics = MetricsPoller(metrics_port)
        httpd.loadgen = LoadGenerator(httpd.metrics, onion, socks_port)
        httpd.allow_reuse_port = True
        httpd.serve_forever()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("torrc")
    args = parser.parse_args()
    main(args.torrc)
