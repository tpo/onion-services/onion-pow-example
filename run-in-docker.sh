#!/bin/sh
set -ve
tag=onion-pow-example
docker build . -t $tag
docker run -it --rm $tag
